package com.classofjava.bank.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Account {
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	private long id;
	@Column(unique = true)
	private String accountNo;
	private String holderName;
	private long balance;
	@OneToOne
	@JoinColumn(name="fk_debit_id")
    private DebitCard debitCard;
	@OneToMany(mappedBy="account")
	private Set<Transaction> passbook;
	
	public Set<Transaction> getPassbook() {
		if (passbook == null) {
			this.passbook=new LinkedHashSet<>();
		}
		return this.passbook;
	}
	public Account() {
		super();
	}
	public Account(String accountNo, String holderName, long balance) {
		super();
		this.accountNo = accountNo;
		this.holderName = holderName;
		this.balance = balance;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	

	public DebitCard getDebitCard() {
		return debitCard;
	}
	public void setDebitCard(DebitCard debitCard) {
		this.debitCard = debitCard;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", accountNo=" + accountNo
				+ ", holderName=" + holderName + ", balance=" + balance + "]";
	}

	
	
	

}
