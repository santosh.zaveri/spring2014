package com.classofjava.bank.service.impl;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classofjava.bank.model.Account;
import com.classofjava.bank.model.DebitCard;
import com.classofjava.bank.model.Transaction;
import com.classofjava.bank.repository.BankRepository;
import com.classofjava.bank.repository.DebitCardRepository;
import com.classofjava.bank.repository.TransactionRepository;
import com.classofjava.bank.service.BankService;

@Service("bankService")
public class BankServiceImpl implements BankService {
	@Autowired
	private BankRepository bankRepository;

	@Autowired
	private DebitCardRepository debitCardRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	private Account getAccount(String acNo) {
		return bankRepository.findByAccountNo(acNo);
	}

	@Transactional
	public String openAccount(String acNo, String name, long openingBalance, String str) {
		DebitCard debitCard = new DebitCard(str,
				new java.util.Date());
		Account account = new Account();
		account.setHolderName(name);
		account.setAccountNo(acNo);
		account.setBalance(openingBalance);
//		account.setDebitCard(debitCard);
//		debitCard.setAccount(account);		
		debitCardRepository.save(debitCard);
		bankRepository.save(account);
		return acNo;
	}

	@Override
	@Transactional
	public void deposit(String acNo, long amount) {
		Account account = getAccount(acNo);
		account.setBalance(account.getBalance() + amount);
		bankRepository.save(account);
		Transaction tr = new Transaction(account, new Date(System.currentTimeMillis()), true, amount);
		transactionRepository.save(tr);
//		account.getPassbook().add(tr);
	}

	@Override
	@Transactional
	public boolean withdraw(String acNo, long amount) {
		Account account = getAccount(acNo);
		if (account.getBalance() < amount) {
			return false;
		}
		account.setBalance(account.getBalance() - amount);
		bankRepository.save(account);
		Transaction tr = new Transaction(account, new Date(System.currentTimeMillis()), false, amount);
		transactionRepository.save(tr);
//		account.getPassbook().add(tr);
		return true;
	}

	@Override
	public String getAccountDetails(String acNo) {
		Account account = getAccount(acNo);
		return account.toString();
	}

	@Override
	public String getPassbookDetails(String acNo) {
		Account account = getAccount(acNo);
		Set<Transaction> passbook = account.getPassbook();
		StringBuilder output = new StringBuilder();
		output.append("Passbook of :"+account.getHolderName()+"\n");
		for (Transaction tr : passbook) {
			output.append(String.format("%dc\t%s\t%15.2d\n", tr.getDateOfTransaction(), tr.isCredit()?"Credit":"Debit", tr.getAmount()));
		}
		return null;
	}

}
