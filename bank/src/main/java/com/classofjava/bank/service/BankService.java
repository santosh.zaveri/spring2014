package com.classofjava.bank.service;

public interface BankService{
    public String openAccount(String acNo, String name, long openingBalance, String str);
    public void deposit(String acNo, long amount);
    public boolean withdraw(String acNo, long amount);
    public String getAccountDetails(String acNo);
    public String getPassbookDetails(String acNo);
}
