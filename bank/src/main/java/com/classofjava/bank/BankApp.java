package com.classofjava.bank;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.classofjava.bank.service.BankService;

public class BankApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("META-INF/applicationContext.xml");
	    BankService bankService = ctx.getBean("bankService", BankService.class);
	    bankService.openAccount("sB1001", "FIRST ACCOUNT",1000,"1111222233334444");
	    String acNo="sB1001";
	    long amount = 5000;
	    bankService.deposit(acNo, amount);
	    System.out.println(bankService.getAccountDetails(acNo));
	     System.out.println(bankService.withdraw(acNo, 6000));
	    System.out.println(bankService.withdraw(acNo, 1000));
	    System.out.println(bankService.getAccountDetails(acNo));
	     System.out.print(bankService.getPassbookDetails(acNo));
	     ((AbstractApplicationContext) ctx).registerShutdownHook();
	}
}
