package com.classofjava.bank.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.classofjava.bank.model.DebitCard;


public interface DebitCardRepository extends  JpaRepository<DebitCard, Long>
{
   
   public DebitCard findByCardNo(@Param(value="cardNo")String cardNo);
}
