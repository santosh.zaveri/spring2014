package com.classofjava.bank.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.classofjava.bank.model.Account;


public interface BankRepository extends  JpaRepository<Account, Long>
{
   
   public Account findByAccountNo(@Param(value="accountNo")String accountNo);
}
