package com.classofjava.bank.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.classofjava.bank.model.Transaction;


public interface TransactionRepository extends  JpaRepository<Transaction, Long>
{
   
//   public Set<Transaction> findByAccountNo(@Param(value="accountNo")String accountNo);
}
