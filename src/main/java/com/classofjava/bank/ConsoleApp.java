package com.classofjava.bank;


import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.classofjava.bank.entity.Account;
import com.classofjava.bank.services.BankService;

public class ConsoleApp {
	 static Logger log = Logger.getLogger(ConsoleApp.class.getName());
	 public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/applicationContext.xml");
		BankService bankService = context.getBean(BankService.class);
		Account account = new Account();
		//("SV1001", "first Account", 1000);
		bankService.openAccount(account);
		System.out.println("after update : " + account.toString());
		bankService.deposit(account, 500);
		System.out.println("after update : " + account.toString());
	
		System.out.println("successfully completed");
	}

}
