package com.classofjava.bank.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.classofjava.bank.entity.Account;
import com.classofjava.bank.repository.AccountRepository;
import com.classofjava.bank.services.BankService;

@Service 
public class BankServiceImpl implements BankService {
	
    @Autowired
    private AccountRepository accountRepository;
    

	public void openAccount(Account account) {
         accountRepository.save(account);
    }

	public void deposit(Account account, long amt) {
		account.setBalance(account.getBalance()+amt);
     	accountRepository.save(account);

	}


/*	public Account find(Integer acId) {
		return accountRepository.findOne(acId);
	}*/
}
