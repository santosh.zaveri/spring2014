package com.classofjava.bank.services;

import com.classofjava.bank.entity.Account;

public interface BankService {
	public void openAccount(Account account);
	public void deposit(Account account, long amt);
	
}
