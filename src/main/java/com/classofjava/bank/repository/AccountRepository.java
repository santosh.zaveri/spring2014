package com.classofjava.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.classofjava.bank.entity.Account;

public interface AccountRepository extends JpaRepository<Account, String> {

	
}
